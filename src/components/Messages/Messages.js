import React from 'react';
import Message from '../../containers/Message/Message';

import './Messages.css';

const messages = (props) => {
    let date;

    const messages = props.messages.map((message, index) => {
        return (
            <React.Fragment key={message.id}>
                {index === 0
                    ? (() => {
                          date = new Date(message.createdAt)
                              .toString()
                              .split(' ');

                          const month = date[1];

                          const day = date[2];

                          date = `${month} ${day}`;

                          return (
                              <div className='separator'>
                                  <span>{date}</span>
                              </div>
                          );
                      })()
                    : null}
                {index > 0
                    ? (() => {
                          let messageCreatedAt = new Date(message.createdAt)
                              .toString()
                              .split(' ');

                          const month = messageCreatedAt[1];

                          const day = messageCreatedAt[2];

                          messageCreatedAt = `${month} ${day}`;

                          if (messageCreatedAt !== date) {
                              date = messageCreatedAt;
                              return (
                                  <div className='separator'>
                                      <span>{messageCreatedAt}</span>
                                  </div>
                              );
                          }
                      })()
                    : null}
                <Message
                    text={message.text}
                    avatarUrl={message.avatar}
                    createdAt={message.createdAt}
                    editedAt={message.editedAt}
                    userName={message.user}
                    id={message.id}
                />
            </React.Fragment>
        );
    });
    return <ul className='messages'>{messages}</ul>;
};

export default messages;
