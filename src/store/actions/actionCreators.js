import * as actionTypes from './actionTypes';

export const stopLoading = () => ({
    type: actionTypes.STOP_LOADING,
});

export const updateMessagesWithFetched = (messagesArray) => ({
    type: actionTypes.UPDATE_MESSAGES_WITH_FETCHED,
    messages: messagesArray,
});

export const submitMessage = (messageObject) => ({
    type: actionTypes.SUBMIT_MESSAGE,
    message: messageObject,
});

export const deleteMessage = (messageId) => ({
    type: actionTypes.DELETE_MESSAGE,
    messageId: messageId,
});

export const editMessage = (messageId, messageText) => ({
    type: actionTypes.EDIT_MESSAGE,
    messageId: messageId,
    messageText: messageText,
});

export const changeModalStatus = (messageText, messageId) => ({
    type: actionTypes.CHANGE_MODAL_STATUS,
    messageText,
    messageId,
});

export const changeModalText = (newText) => ({
    type: actionTypes.CHANGE_MODAL_TEXT,
    newText,
});

export const submitModalText = (messageText, messageId) => ({
    type: actionTypes.SUBMIT_MODAL_TEXT,
    messageText,
    messageId,
});

export const inputChanged = (messageText) => ({
    type: actionTypes.INPUT_CHANGED,
    messageText: messageText,
});
