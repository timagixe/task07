import * as actionTypes from '../actions/actionTypes';

const initialState = {
    messages: [],
    loading: true,
    showModal: false,
};

const stopLoading = (state) => ({
    ...state,
    loading: !state.loading,
});

const updateMessagesWithFetched = (state, action) => ({
    ...state,
    messages: action.messages,
});

const submitMessage = (state, action) => {
    return {
        ...state,
        messages: [...state.messages].concat(action.message),
    };
};

const deleteMessage = (state, action) => ({
    ...state,
    messages: state.messages.filter(
        (message) => message.id !== action.messageId
    ),
});

const editMessage = (state, action) => ({
    ...state,
    messages: state.messages.map((message) => {
        if (message.id === action.messageId) {
            message.text = action.messageText;
            message.editedAt = new Date().toString();
        }
        return message;
    }),
});

const submitModalText = (state, action) => {
    return {
        ...state,
        messages: state.messages.map((message) => {
            if (message.id === action.messageId) {
                message.text = action.messageText;
                message.editedAt = new Date().toString();
            }
            return message;
        }),
    };
};

const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STOP_LOADING:
            return stopLoading(state);
        case actionTypes.UPDATE_MESSAGES_WITH_FETCHED:
            return updateMessagesWithFetched(state, action);
        case actionTypes.SUBMIT_MESSAGE:
            return submitMessage(state, action);
        case actionTypes.DELETE_MESSAGE:
            return deleteMessage(state, action);
        case actionTypes.EDIT_MESSAGE:
            return editMessage(state, action);
        case actionTypes.SUBMIT_MODAL_TEXT:
            return submitModalText(state, action);
        default:
            return state;
    }
};

export default mainReducer;
