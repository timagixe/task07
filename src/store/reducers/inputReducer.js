import * as actionTypes from '../actions/actionTypes';

const initialState = {
    inputValue: '',
};

const inputChanged = (state, action) => ({
    ...state,
    inputValue: action.messageText,
});

const inputReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INPUT_CHANGED:
            return inputChanged(state, action);
        default:
            return state;
    }
};

export default inputReducer;
