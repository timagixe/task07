import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import Chat from './containers/Chat/Chat';
import mainReducer from './store/reducers/mainReducer';
import modalReducer from './store/reducers/modalReducer';
import inputReducer from './store/reducers/inputReducer';

import './App.css';

const rootReducer = combineReducers({
    mainReducer: mainReducer,
    modalReducer: modalReducer,
    inputReducer: inputReducer,
});

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function App() {
    return (
        <Provider store={store}>
            <Chat />;
        </Provider>
    );
}

export default App;
