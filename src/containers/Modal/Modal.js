import React from 'react';
import './Modal.css';
import Backdrop from '../../components/Backdrop/Backdrop';

import * as actions from '../../store/actions/actionCreators';
import { connect } from 'react-redux';

const modal = (props) => {
    let { onCloseModal, onTextSubmit, messageText, messageId } = props;

    const getFocusOnInput = () => {
        setTimeout(() => {
            document.getElementById('editInput').focus();
        }, 10);
    };

    const render =
        props.show === false ? null : (
            <React.Fragment>
                <Backdrop show={props.show} clicked={props.onCloseModal} />
                <div
                    className='modal'
                    style={{
                        transform: props.show
                            ? 'translateY(0)'
                            : 'translateY(-100vh)',
                        opacity: props.show ? 1 : 0,
                    }}
                >
                    <input
                        id='editInput'
                        placeholder='Type your message here...'
                        value={`${props.messageText}`}
                        onChange={(event) => {
                            props.onTextChange(event.target.value);
                        }}
                        onKeyDown={(event) => {
                            if (event.key === 'Enter') {
                                if (messageText.trim() === '') {
                                    alert('MESSAGE CANNOT BE EMPTY');
                                } else {
                                    onTextSubmit(messageText, messageId);
                                    onCloseModal();
                                }
                            }

                            if (event.key === 'Escape') onCloseModal();
                        }}
                    />
                    <button
                        onClick={() => {
                            if (messageText.trim() === '') {
                                alert('MESSAGE CANNOT BE EMPTY');
                            } else {
                                onTextSubmit(messageText, messageId);
                                onCloseModal();
                            }
                        }}
                    >
                        Send
                    </button>
                </div>
                {getFocusOnInput()}
            </React.Fragment>
        );

    return render;
};

const mapStateToProps = (state) => ({
    messageText: state.modalReducer.messageText,
    messageId: state.modalReducer.messageId,
});

const mapDispatchToProps = (dispatch) => ({
    onCloseModal: () => dispatch(actions.changeModalStatus()),
    onTextChange: (newText) => dispatch(actions.changeModalText(newText)),
    onTextSubmit: (messageText, messageId) =>
        dispatch(actions.submitModalText(messageText, messageId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(modal);
