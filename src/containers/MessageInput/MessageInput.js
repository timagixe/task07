import React from 'react';
import * as actions from '../../store/actions/actionCreators';

import './MessageInput.css';
import { connect } from 'react-redux';

class MessageInput extends React.Component {
    inputChangeHandler(event) {
        this.props.onInputChange(event.target.value);
    }

    keyDownHandler(event, messages = this.props.messages) {
        const userOwnMessages = messages.filter(
            (message) => message.user === 'Yourself'
        );

        if (userOwnMessages.length > 0 && event.key === 'ArrowUp') {
            const { text, id } = userOwnMessages[userOwnMessages.length - 1];
            this.props.onShowModal(text, id);
        }

        if (event.key === 'Enter') {
            if (this.props.inputValue.trim() === '') {
                alert('Message cannot be empty');
            } else {
                this.props.onSubmitMessage({
                    id: Math.floor(Math.random() * 10000),
                    text: this.props.inputValue,
                    user: 'Yourself',
                    avatar:
                        'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
                    userId: 'userIdHere',
                    editedAt: '',
                    createdAt: new Date().toString(),
                });

                const inputValue = document.getElementsByClassName(
                    'message_input'
                )[0];
                inputValue.value = '';

                this.props.onInputChange('');
            }
        }
    }

    render() {
        return (
            <div className='bottom_wrapper clearfix'>
                <div className='message_input_wrapper'>
                    <input
                        onKeyDown={(event) => this.keyDownHandler(event)}
                        className='message_input'
                        placeholder='Type your message here...'
                        onChange={(event) => this.inputChangeHandler(event)}
                    />
                </div>
                <div
                    className='send_message'
                    onClick={(event) => {
                        if (this.props.inputValue.trim() === '') {
                            alert('Message cannot be empty');
                        } else {
                            this.props.onSubmitMessage({
                                id: Math.floor(Math.random() * 10000),
                                text: this.props.inputValue,
                                user: 'Yourself',
                                avatar:
                                    'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
                                userId: 'userIdHere',
                                editedAt: '',
                                createdAt: new Date().toString(),
                            });

                            const inputValue = document.getElementsByClassName(
                                'message_input'
                            )[0];
                            inputValue.value = '';

                            this.props.onInputChange('');
                        }
                    }}
                >
                    <div className='icon'></div>
                    <div className='text'>Send</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    messages: state.mainReducer.messages,
    inputValue: state.inputReducer.inputValue,
});

const mapDispatchToProps = (dispatch) => ({
    onSubmitMessage: (messageObject) =>
        dispatch(actions.submitMessage(messageObject)),
    onShowModal: (currentMessageText, currentMessageId) =>
        dispatch(
            actions.changeModalStatus(currentMessageText, currentMessageId)
        ),
    onInputChange: (messageText) => dispatch(actions.inputChanged(messageText)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
